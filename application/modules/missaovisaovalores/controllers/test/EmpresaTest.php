<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/missaovisaovalores/libraries/Empresa.php';


class EmpresaTest extends Toast{
    private $empresa;

    function __construct(){
        parent::__construct('EmpresaTest');
    }

    function _pre(){
        $this->empresa = new Empresa();
    }

    function test_carrega_lista_de_empresas(){
        $v = $this->empresa->lista();
        $this->_assert_equals(4, sizeof($v), "Número de empresas incorreto");
    }


}