<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class MVVEmpresaDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo_test'){
        parent::__construct('missaovisaovalores', $table);
    }

    function getData($index = -1){
        $data[0]['empresa_id'] = 5;
        $data[0]['empresa'] = 'Missão';
        $data[0]['descricao'] = 'Uma missão com uma descrição curta';

        $data[1]['empresa_id'] = 6;
        $data[1]['empresa'] = 'Visão';
        $data[1]['descricao'] = 'Precisa informar mais sobre a visão';

        $data[2]['empresa_id'] = 7;
        $data[2]['empresa'] = 'Valores';
        $data[2]['descricao'] = 'Detalhe melhor seus valores';
        return $index > -1 ? $data[$index] : $data;
    }

}