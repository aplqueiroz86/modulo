<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/missaovisaovalores/libraries/MVVEmpresa.php';
include_once APPPATH . 'modules/missaovisaovalores/controllers/test/builder/MVVEmpresaDataBuilder.php';

class MVVEmpresaTest extends Toast{
    private $builder;
    private $empresa;

    function __construct(){
        parent::__construct('MVVEmpresaTest');
    }

    function _pre(){
        $this->builder = new MVVEmpresaDataBuilder();
        $this->empresa = new MVVEmpresa();
    }

    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->empresa, "Erro na criação da missão,visão e valores");
    }

    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo_test', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->empresa->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        $task = $this->empresa->get(array('id' => 1))[0];
        $this->_assert_equals($data['empresa'], $task['empresa']);
        $this->_assert_equals($data['descricao'], $task['descricao']);
        $this->_assert_equals($data['empresa_id'], $task['empresa_id']);

        $id2 = $this->empresa->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->empresa->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        
        $v = array('empresa' => 'vetor incompleto');
        $id = $this->empresa->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->empresa->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $task = $this->empresa->get(array('empresa' => 'Visão', 'empresa_id' => 6))[0];
        $this->_assert_equals('Visão', $task['empresa'], "Erro na visão da empresa");
        $this->_assert_equals(6, $task['empresa_id'], "Erro no id da empresa");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();
        
        $task1 = $this->empresa->get(array('id' => 2))[0];
        $this->_assert_equals('Visão', $task1['empresa'], "Erro na visão da empresa");
        $this->_assert_equals(6, $task1['empresa_id'], "Erro no id da empresa");
        
        $task1['empresa'] = 'Nova Visão da Empresa';
        $task1['empresa_id'] = 8;
        $this->empresa->insert_or_update($task1);

        $task2 = $this->empresa->get(array('id' => 2))[0];
        $this->_assert_equals($task1['empresa'], $task2['empresa'], "Erro na descrição principal");
        $this->_assert_equals($task1['empresa_id'], $task2['empresa_id'], "Erro no id da empresa");
    }
    
    function test_se_o_titulo_da_descrição_principal_é_o_mesmo_gravado_no_banco(){
        $this->builder->clean_table();
        $this->builder->build();
        
        $task1 = $this->empresa->get(array('id' => 2))[0];
        $this->_assert_equals('Visão da Empresa', $task1['empresa'], "Erro na descrição principal");
       
       
 
    }

}