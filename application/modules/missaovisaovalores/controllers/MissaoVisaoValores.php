<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class MissaoVisaoValores extends MY_Controller{

    public function __construct(){
        $this->load->model('MissaoVisaoValoresModel', 'model');
    }

    public function index(){
        $data['titulo'] = 'Lista das Empresas';
        $data['rotulo_botao'] = 'Nova Empresa';
        $data['form_subject'] = 'nova_empresa';
        $data['show_form'] = false;
        $data['topo'] = $this->load->view('topo', $data, true);
        $data['formulario'] = $this->load->view('formfixo', $data, true);
        $data['lista'] = $this->model->fake_list();

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }


   
    public function criar($empresa_id){
        $this->add_script('missaovisaovalores/mascara');
        
        $this->validate_id($empresa_id);
        $empresa = $this->model->nome_empresa($empresa_id);
        $data['show_form'] = $this->model->nova_descricao($empresa_id);
        $data['home'] = true;
        $data['titulo'] = "Descrição da Missão, Visão, Valores da Empresa: $empresa";
        $data['rotulo_botao'] = 'Nova Descrição';
        $data['form_subject'] = 'nova_descicao';
        $data['topo'] = $this->load->view('topo', $data, true);
        $data['formulario'] = $this->load->view('form_descricao', $data, true);
        $data['lista'] = $this->model->lista_descricao($empresa_id);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

  /**
   * @param int mvv_id: id da missão,visão,valores
   */
    public function editar($mvv_id){
        $this->validate_id($mvv_id);
        $empresa_id = $this->model->edita_descricao($mvv_id);
        $empresa = $this->model->nome_empresa($empresa_id);
        $data['show_form'] = true;

        $data['titulo'] = "Editar descrição da Missão, Visão, Valores da Empresa: $empresa";
        $data['rotulo_botao'] = 'Nova Descrição';
        $data['form_subject'] = 'nova_descicao';
        $data['topo'] = $this->load->view('topo', $data, true);
        $data['formulario'] = $this->load->view('missaovisaovalores/form_descricao', $data, true);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }


    public function deletar($mvv_id){
        $this->validate_id($mvv_id);
        $data = $this->model->deleta_descricao($mvv_id);       

        $data['home'] = true;
        $data['titulo'] = "Remover descrição da Missão, Visão, Valores da Empresa";
        $data['empresa'] = $this->model->nome_empresa($data['empresa_id']);
        $data['topo'] = $this->load->view('topo', $data, true);
        $html = $this->load->view('confirm_delete', $data, true);
        $this->show($html);
    }







    

}