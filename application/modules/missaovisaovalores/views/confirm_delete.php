<div class="container mt-5">
    <?= $topo ?>
    <div class="row mt-5">
        <div class="card col-md-8 mx-auto">
            <div class="card-body">
                <h3 class="card-title">Confirmação de remoção</h3><br>
                <p class="card-text">Deseja, realmente, remover a descrição ? </p>

                <form id="delete-task-form" method="POST">
                        <a class="btn btn-light cancel-btn"  onclick="JavaScript: window.history.back();" >Cancelar</a>
                        <a class="delete-btn btn btn-warning" onclick="document.getElementById('delete-task-form').submit();">
                            Remover
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>