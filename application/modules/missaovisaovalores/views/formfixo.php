<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="nova_empresa">
    <div class="card">
        <div class="card-header"><h4>Dados Básicos da Empresa</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4">
                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome da Empresa">
                    </div>                    
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <input type="text" name="cnpj" value="<?= set_value('cnpj') ?>" class="form-control mb-4" placeholder="CNPJ">
                    </div>                    
                </div>
                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-warning">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>