<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="nova_descricao">
    <div class="card">
        <div class="card-header"><h4>Descrição da Missão, Visão e Valores da Empresa</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4" id="task-form">
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <input type="text" name="empresa" value="<?= set_value('empresa') ?>" class="form-control" placeholder="Titulo Principal (Frase de Efeito)">
                    </div>                   
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-12"><textarea name="descricao" class="form-control" id="descricao" rows="4" placeholder="Descreva sua Missão, Visão e Valores"><?= set_value('descricao') ?></textarea></div>
                </div>

                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-warning" onclick="document.getElementById('task-form').submit();">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>