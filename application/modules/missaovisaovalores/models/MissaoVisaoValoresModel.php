<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/buttons/EditDeleteButtonGroup.php';
include_once APPPATH.'libraries/component/Table.php';

class MissaoVisaoValoresModel extends CI_Model{

   
    public function fake_list(){
        $this->load->library('Empresa');
        $data = $this->empresa->lista();
        $header = array('#', 'Nome da Empresa', 'CNPJ');
        $table = new Table($data, $header);
        $table->action('missaovisaovalores/criar');
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        return $table->getHTML();
    }

   
    public function lista_descricao($empresa_id){
        $header = array('#', 'Título', 'Descrição');
        $this->load->library('MVVEmpresa', null, 'missaovisaovalores');
        $this->missaovisaovalores->cols(array('id', 'empresa', 'descricao'));
        $data = $this->missaovisaovalores->get(array('empresa_id' => $empresa_id));
        if(! sizeof($data)) return '';
        
        $table = new Table($data, $header);
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        
        $edbg = new EditDeleteButtonGroup('missaovisaovalores');
        $table->use_action_button($edbg);
        return $table->getHTML();
    }

   


    public function nova_descricao($empresa_id){
        if(! sizeof($_POST)) return;
        $this->load->library('Validator', null, 'valida');

        if($this->valida->form_descricao()){
            $this->load->library('MVVEmpresa', null, 'missaovisaovalores');
            $data = $this->input->post();
            $data['empresa_id'] = $empresa_id;
            $this->missaovisaovalores->insert($data);
        }
        else return true;
    }



 
    public function edita_descricao($empresa_id){
        $this->load->library('MVVEmpresa', null, 'missaovisaovalores');
        $this->load->library('Validator', null, 'valida');
        $task = $this->missaovisaovalores->get(array('id' => $empresa_id));

        if(sizeof($_POST) && $this->valida->form_descricao()){
            $data = $this->input->post();
            $data['id'] = $empresa_id;
            $id = $this->missaovisaovalores->insert_or_update($data);
            if($id) redirect('missaovisaovalores/criar/'.$task[0]['empresa_id']);
        }
        else {
            foreach ($task[0] as $key => $value)
                $_POST[$key] = $value;
            return $_POST['empresa_id'];
        }
    }
   
    public function deleta_descricao($empresa_id) {
        $this->load->library('MVVEmpresa', null, 'missaovisaovalores');
        $task = $this->missaovisaovalores->get(array('id' => $empresa_id));
        
        if(sizeof($_POST)) {
            if($this->missaovisaovalores->delete(array('id' => $empresa_id)))
                redirect('missaovisaovalores/criar/'.$task[0]['empresa_id']);
        }
        else return $task[0];
    }

   
    public function nome_empresa($empresa_id){
        $this->load->library('Empresa');
        return $this->empresa->nome($empresa_id);
    }



}