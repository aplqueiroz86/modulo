<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function form_descricao(){
        $this->form_validation->set_rules('empresa', 'empresa', 'trim|required|min_length[5]|max_length[128]');
        $this->form_validation->set_rules('descricao', 'descricao', 'trim|required|min_length[20]|max_length[500]');
        return $this->form_validation->run();
    }

}