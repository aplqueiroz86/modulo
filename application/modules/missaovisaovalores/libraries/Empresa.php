<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/Dao.php';

class Empresa extends Dao{

    function __construct(){
        parent::__construct('empresa');
    }
    
   
    public function lista(){
        return array(
            array('id' => 1, 'Consultoria Lima', '62.463.260/0001-50'),
            array('id' => 2, 'Stillus Informática', '62.463.260/0001-44'),
            array('id' => 3, 'Virtual Image', '62.463.260/0001-05'),
            array('id' => 4, 'Physicus', '62.463.260/0001-22'),
        );
    }
        
  
    public function nome($empresa_id){
        $v = array('', 'Consultoria Lima', 'Stillus Informática', 'Virtual Image', 'Physicus');
        return $v[$empresa_id];
    }
}