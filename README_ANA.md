## AT03 - HMVC - PROJETO FINAL - MISSÃO,VISÃO E VALORES

####Executar o código do módulo missaovisaovalores
1. Clone este repositório em um diretório chamado **modulo**
2. Importe para o MySQL o arquivo missaovisaovalores.sql que se encontra na pasta sql
3. Digite no navegador **http://localhost/modulo/missaovisaovalores**
4. Na home ao clicar em Nova Empresa irá conseguir visualizar um formulário fictício, não está adicionando no banco.
5. Ao clicar em uma linha, conseguirá ver a missão desta empresa
6. O botão Nova Descrição, está sem ação.
7. Ao clicar em editar é possível editar o titulo principal e a descrição da missão,visão ou valor da empresa.
8. Ao clicar em excluir, abrirá uma nova página para sua confirmação, o botão cancelar retorna a página anterior, porém o botão remover está sem ação efetiva.



####Executar os testes de unidade do módulo
1. O teste deste módulo está em  **missaovisaovalores/controllers/test**
2. No módulo missaovisaovalores existe um teste chamado MVVEmpresaTest; execute-o acessando **http://localhost/modulo/missaovisaovalores/test/MVVEmpresaTest**
3. Para executar o teste de regressão do módulo, acesse **http://localhost/modulo/missaovisaovalores/test/all**
