
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- Database: `lp2_modulo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `missaovisaovalores`
--

CREATE TABLE `missaovisaovalores` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `empresa` varchar(128) NOT NULL,
  `descricao` varchar(512) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
